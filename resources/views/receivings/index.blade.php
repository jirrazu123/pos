@extends('main')


@section('content')

<div class="container-fluid">
   <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><span class="glyphicon glyphicon-inbox" aria-hidden="true"></span></div>

            <div class="panel-body">


                <div class="row" ng-controller="SearchItemCtrl">

                    <div class="col-md-3">
                        <label><input ng-model="searchKeyword" class="form-control"></label>

                        <table class="table table-hover">
                        <tr ng-repeat="item in items  | filter: searchKeyword | limitTo:10">

                        <td></td><td><button class="btn btn-primary btn-xs" type="button" ng-click="addReceivingTemp(item,newreceivingtemp)"><span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span></button></td>

                        </tr>
                        </table>
                    </div>

                    <div class="col-md-9">

                        <div class="row">

                          
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="invoice" class="col-sm-3 control-label"></label>
                                        <div class="col-sm-9">
                                        
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="employee" class="col-sm-3 control-label"></label>
                                        <div class="col-sm-9">
                                        
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="supplier_id" class="col-sm-4 control-label"></label>
                                        <div class="col-sm-8">
                                        
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="payment_type" class="col-sm-4 control-label"></label>
                                        <div class="col-sm-8">
                                        
                                        </div>
                                    </div>
                                </div>
                        </div>

                        <table class="table table-bordered">
                            <tr>
                            	<th></th>
                            	<th></th>
                            	<th></th>
                            	<th></th>
                            	<th></th>
                            	<th>&nbsp;</th>
                            </tr>
                            <tr ng-repeat="newreceivingtemp in receivingtemp">
	                            <td></td>
	                            <td></td>
	                            <td></td>
	                            <td></td>
	                            <td></td>
	                            <td><button class="btn btn-danger btn-xs" type="button" ng-click="removeReceivingTemp(newreceivingtemp.id)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></td>
                            </tr>
                        </table>

                        <div class="row">


                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="total" class="col-sm-5 control-label"></label>
                                        <div class="col-sm-7">
                                            <div class="input-group">
                                                <div class="input-group-addon">$</div>
                                                <input type="text" class="form-control" id="amount_tendered"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div>&nbsp;</div>
                                    <div class="form-group">
                                        <label for="employee" class="col-sm-4 control-label"></label>
                                        <div class="col-sm-8">
                                        <input type="text" class="form-control" name="comments" id="comments" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="supplier_id" class="col-sm-4 control-label"></label>
                                        <div class="col-sm-8">
                                            <p class="form-control-static"><b></b></p>
                                        </div>
                                    </div>
                                    <div>&nbsp;</div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary btn-block"></button>
                                        </div>
                                    </div>
                                </div>
                        </div>
                           



                    </div>

                </div>

            </div>
            </div>
        </div>
    </div>
</div>

@endsection