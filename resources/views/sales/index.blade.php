@extends('main')


@section('content')

<div class="col-md-3">
	<div class="well">
		<div class="row">
			<div class="col-sm-12">
				<button type="button" class="btn btn-primary btn-sm">Add New Items</button>
			</div>
		</div>
		<div>&nbsp;</div>
		<div class="row">
			<div class="col-sm-12">
				<button type="button" class="btn btn-primary btn-sm">Add New Items</button>
			</div>
			
		</div>
		<div>&nbsp;</div>
		<div class="row">
			<div class="col-sm-12">
				<button type="button" class="btn btn-primary btn-sm">Add New Items</button>
			</div>
			
		</div>
		<div>&nbsp;</div>
		<div class="row">
			<div class="col-sm-12">
				<button type="button" class="btn btn-primary btn-sm">Add New Items</button>
			</div>
			
		</div>
				
	</div>
</div>
<div class="col-md-9">
	<div class="row">
		<div class="panel panel-primary">
			<div class="panel-heading"><span class="glyphicon glyphicon-inbox"></span>
				Sellings
			</div>
			<div class="panel-body">
				<div class="row" ng-controller="SearchItemCtrl">

					<div class="col-md-3">
						<label>Search Item
							<input ng-model="searchKeyword" class="form-control" type="" name="">
						</label>
						<table class="table table-hober">
							<tr ng-repeat="item in items  | filter: searchKeyword | limitTo:10">
								<td>Example item</td>
								<td>
									<button class="btn btn-success btn-xs" type="button" ng-click="addSaleTemp(item, newsaletemp)" ><span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span></button>
								</td>
							</tr>
						</table>
					</div>

					<div class="col-md-9">
						<div class="row">
							<div class="col-md-5">
								<div class="form-group">
									<label for="invoice" class="col-sm-3 control-label">
										Invoice
									</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" name="" readonly/>
									</div>
								</div>

								<div>&nbsp;</div>

								<div class="form-group">
									<label for="invoice" class="col-sm-3 control-label">
										Employee
									</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" name="" readonly/>
									</div>
								</div>

							</div>

							<div class="col-md-7">
								<div class="form-group">
									<label for="customer_id" class="col-sm-4 control-label">Customer</label>
                                        <div class="col-sm-8">
                                        	<input type="#" name="#">
                                        </div>
								</div>

								<div>&nbsp;</div>

								<div class="form-group">
									<label for="customer_id" class="col-sm-4 control-label">Customer</label>
                                        <div class="col-sm-8">
                                        	<input type="#" name="#">
                                        </div>
								</div>

							</div>
						</div>
						<div>&nbsp;</div>
						<table class="table table-bordered">
							<tr>
								<th>Item Id</th>
								<th>Item Name</th>
								<th>Price</th>
								<th>Quantity</th>
								<th>Total</th>
								<th>&nbsp;</th>
							</tr>
							<tr ng-repeat="newsaletemp in saletemp">
								<td></td>
								<td></td>
								<td></td>
								<td><input type="text" style="text-align: center;" autocomplete="off" name="quantity" ng-change="updateSaleTemp(newsaletemp)" ng-model="newsaletemp.quantity"  size="2"></td>
								<td></td>
								<td><button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></td>
							</tr>
						</table>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
                                    <label for="total" class="col-sm-4 control-label">Add payment</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <div class="input-group-addon">$</div>
                                            <input type="text" class="form-control" id="add_payment" ng-model="add_payment"/>
                                        </div>
                                    </div>
                                </div>
                                <div>&nbsp;</div>
                                <div class="form-group">
                                    <label for="employee" class="col-sm-4 control-label">Comment</label>
                                    <div class="col-sm-8">
                                    <input type="text" class="form-control" name="comments" id="comments" />
                                    </div>
                                </div>

							</div>

							<div class="col-md-6">
								<div class="form-group">
                                    <label for="supplier_id" class="col-sm-4 control-label">Total</label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static"><b></b></p>
                                    </div>
                                </div>

                                <div class="form-group">
                                        <label for="amount_due" class="col-sm-4 control-label">Amount Due</label>
                                        <div class="col-sm-8">
                                        <p class="form-control-static"></p>
                                        </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-success btn-block">Submit</button>
                                    </div>
                                </div>

							</div>
						</div>

					</div>

				</div>
			</div>
		</div>
	</div>
</div>


@endsection