

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="tryit.asp-filename=trybs_temp_store&stacked=h.html#">Logo</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class=""><a href="#">Home</a></li>
        
       
        <li class="{{ Request::is('customers') ? "active" : ""}}"><a href="/customers">Customers</a></li>

        <li class="{{Request::is('items')?"active" : ""}}"><a href="/items">Items</a></li>

        <li class="{{ Request::is('suppliers') ? "active" : ""}}"><a href="/suppliers">Suppliers</a></li>

        <li class="{{ Request::is('receivings') ? "active" : ""}}"><a href="/receivings">Receiving</a></li>
        
        <li class="{{ Request::is('sales') ? "active" : ""}}"><a href="/sales">Sales</a></li>
        <li class="{{ Request::is('employees') ? "active" : ""}}"><a href="/employees">Employee</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="tryit.asp-filename=trybs_temp_store&stacked=h.html#"><span class="glyphicon glyphicon-user"></span> Your Account</a></li>
        <li><a href="tryit.asp-filename=trybs_temp_store&stacked=h.html#"><span class="glyphicon glyphicon-shopping-cart"></span> Cart</a></li>
      </ul>
    </div>
  </div>
</nav>

