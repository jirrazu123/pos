@extends('main')


@section('content')

<div class="col-md-12">
	    <div class="box">
		<div class="box-header">

	      <div class="col-md-3">
			<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal2">Add Items</button><br><hr>

		  </div> 
		  

			<div class="modal fade" id="myModal2" role="dialog">
				<div class="modal-dialog">

				  <!-- Modal content-->
				  <div class="modal-content">

				    <div class="modal-header">
				      <button type="button" class="close" data-dismiss="modal">&times;</button>
				      <h4 class="modal-title">Create New Item</h4>
				    </div>
				    
				    <div class="modal-body">
				    {!! Form::open(array('route' => 'items.store', 'class'=>'form-horizontal form-horizontal-option', 'files' => true)) !!}
				  	
				  	 {{ Form::label('upc','UPC:')}}
           			 {{ Form::text('upc', null, array('class'=>'form-control'))}}

				  	 {{ Form::label('item_name','Name:')}}
           			 {{ Form::text('item_name', null, array('class'=>'form-control'))}}

           			 {{ Form::label('size', 'Size:')}}
           			 {{ Form::text('size',null, array('class'=>'form-control'))}}

           			 {{ Form::label('description','Description')}}
           			 {{ Form::textarea('description', null, array('class'=>'form-control'))}}

           			 {{ Form::label('cost_price', 'Cost Price:')}}
           			 {{ Form::text('cost_price', null, array('class'=>'form-control'))}}

           			 {{ Form::label('selling_price','Selling Price')}}
           			 {{ Form::text('selling_price', null, array('class'=>'form-control'))}}

           			 {{ Form::label('quantity','Quantity:')}}
           			 {{ Form::text('quantity', null,array('class'=>'form-control'))}}

           			 {{ Form::label('company_name','Company Name:')}}
           			 {{ Form::text('company_name', null, array('class'=>'form-control'))}}


           			 {{ Form::submit('Submit', array('class' => 'btn btn-success btn-md btn-block', 'style' => 'margin-top: 20px;')) }}


				    {!! Form::close() !!}
				     
					  </div>

					 
					 
					  </div> 
				</div>
				<!--/.modal-dialog-->
			</div>	
		</div>
		 <!-- /.box-header -->

         

	    <div class="box-body">
	 
	    		
	      <table id="example2" class="table table-bordered table-hover">
	        <thead>
	        <tr>
	          <th>Item Id</th>
	          <th>UPC</th>
	          <th>Item Name</th>
	          <th>Size</th>
	          <th>Cost Price</th>
	          <th>Selling Price</th>
	          <th>Quantity</th>
	          <th>Image</th>
	          <th>Action</th>
	        </tr> 
	        </thead>
	        <tbody> 
	       		@foreach($items as $item)
		       		<tr>
		       			<th>{{$item->id}}</th>
		       			<th>{{$item->upc}}</th>
		       			<th>{{$item->name}}</th>
		       			<th>{{$item->size}}</th>
		       			<th>{{$item->cost_price}}</th>
		       			<th>{{$item->selling_price}}</th>
		       			<th>{{$item->quantity}}</th>
		       			<th></th>
		       			<td><a href="#" class="btn btn-default btn-sm">View</a><a href="#" class="btn btn-primary btn-sm">Edit</a><a href="" class="btn btn-danger btn-sm">Delete</a></td>
		       		</tr>

	       		@endforeach
	        
	        </tbody>
	        <tfoot>
	         <tr>
	          <th>Item Id</th>
	          <th>UPC</th>
	          <th>Item Name</th>
	          <th>Size</th>
	          <th>Cost Price</th>
	          <th>Selling Price</th>
	          <th>Quantity</th>
	          <th>Image</th>
	          <th>Action</th>
	        </tr> 
	        </tfoot>
	      </table>
	    </div>
	    <!-- /.box-body -->

	    <div class="box-footer clearfix">
	      <ul class="pagination pagination-sm no-margin pull-right">
	        <li><a href="#">&laquo;</a></li>
	        <li><a href="#">1</a></li>
	        <li><a href="#">2</a></li>
	        <li><a href="#">3</a></li>
	        <li><a href="#">&raquo;</a></li>
	      </ul>
	    </div>
  </div>	  
  <!-- /.box -->
</div>
@endsection