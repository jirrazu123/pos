@extends('main')

@section('title','|Create New Post')



@section('content')

<div class="row">
    <div class="col-md-8 col-md-offset-2">
			 {!! Form::open(array('route' => 'suppliers.store', 'class'=>'form-horizontal form-horizontal-option', 'files' => true)) !!}
		  	
		  	 {{ Form::label('name','Name:')}}
   			 {{ Form::text('name', null, array('class'=>'form-control'))}}

   			 {{ Form::label('email', 'Email:')}}
   			 {{ Form::text('email',null, array('class'=>'form-control'))}}

   			 {{ Form::label('phone_number', 'Phone Number:')}}
   			 {{ Form::text('phone_number', null, array('class'=>'form-control'))}}

   			 {{ Form::label('address','Address:')}}
   			 {{ Form::text('address', null, array('class'=>'form-control'))}}

   			 {{ Form::label('city','Company Name:')}}
   			 {{ Form::text('city', null, array('class'=>'form-control'))}}

   			 {{ Form::label('comments','Comment:')}}
   			 {{ Form::text('comments', null,array('class'=>'form-control'))}}

   			 {{ Form::label('account', 'Account:')}}
   			 {{ Form::text('account',null, array('class'=>'form-control'))}}

   			 {{ Form::submit('Submit', array('class' => 'btn btn-success btn-md btn-block', 'style' => 'margin-top: 20px;')) }}


		    {!! Form::close() !!}

        
    </div>
</div>

@endsection

