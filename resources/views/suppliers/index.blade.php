@extends('main')

@section('title', '| All Posts')

@section('content')

	<div class="row">
		<div class="col-md-9">
			<h1>All Supplier</h1>
		</div>

		<div class="col-md-3">
			<a href="{{route('suppliers.create')}}" class="btn btn-md btn-block btn-info btn-h1-spacing">Create New Supplier</a>
		</div>
		<div class="col-md-12">
			<hr>
		</div>
	</div> <!-- end of .row -->

	<div class="row">
		<div class="col-md-12">
			<table class="table">
				<thead>
					<th>#</th>
					<th>Supplier Name</th>
					<th>Supplier Emil</th>
					<th>Mobile No</th>
					<th>Image</th>
					<th>Action</th>
					
				</thead>

				<tbody>
					@foreach($suppliers as $supplier)
		       		<tr>
		       			<th>{{$supplier->id}}</th>
		       			<th>{{$supplier->name}}</th>
		       			<th>{{$supplier->email}}</th>
		       			<th>{{$supplier->phone_number}}</th>
		       			<th></th>
		       			<td><a href="#" class="btn btn-default btn-sm">View</a><a href="#" class="btn btn-primary btn-sm">Edit</a><a href="" class="btn btn-danger btn-sm">Delete</a></td>
		       		</tr>

		       		@endforeach
				</tbody>
			</table>

		</div>
	</div>

@stop