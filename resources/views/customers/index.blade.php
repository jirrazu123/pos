@extends('main')


@section('content')

<div class="col-md-12">
	    <div class="box">
		<div class="box-header">

	      <div class="col-md-3">
			<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal2">Add Customers</button><br><hr>

		  </div> 
		  

			<div class="modal fade" id="myModal2" role="dialog">
				<div class="modal-dialog">

				  <!-- Modal content-->
				  <div class="modal-content">

				    <div class="modal-header">
				      <button type="button" class="close" data-dismiss="modal">&times;</button>
				      <h4 class="modal-title">Create New Customer</h4>
				    </div>
				    
				    <div class="modal-body">
				    {!! Form::open(array('route' => 'customers.store', 'class'=>'form-horizontal form-horizontal-option', 'files' => true)) !!}
				  	
				  	 {{ Form::label('name','Name:')}}
           			 {{ Form::text('name', null, array('class'=>'form-control'))}}

           			 {{ Form::label('email', 'Email:')}}
           			 {{ Form::text('email',null, array('class'=>'form-control'))}}

           			 {{ Form::label('phone_number', 'Phone Number:')}}
           			 {{ Form::text('phone_number', null, array('class'=>'form-control'))}}

           			 {{ Form::label('address','Address:')}}
           			 {{ Form::text('address', null, array('class'=>'form-control'))}}

           			 {{ Form::label('comment','Comment:')}}
           			 {{ Form::text('comment', null,array('class'=>'form-control'))}}

           			 {{ Form::label('company_name','Company Name:')}}
           			 {{ Form::text('company_name', null, array('class'=>'form-control'))}}

           			 {{ Form::label('account', 'Account:')}}
           			 {{ Form::text('account',null, array('class'=>'form-control'))}}

           			 {{ Form::submit('Submit', array('class' => 'btn btn-success btn-md btn-block', 'style' => 'margin-top: 20px;')) }}


				    {!! Form::close() !!}
				     
					  </div>

					 
					 
					  </div> 
				</div>
				<!--/.modal-dialog-->
			</div>	
		</div>
		 <!-- /.box-header -->

         

	    <div class="box-body">
	 
	    		
	      <table id="example2" class="table table-bordered table-hover">
	        <thead>
	        <tr>
	          <th>Customer ID</th>
	          <th>Name</th>
	          <th>Email</th>
	          <th>Phone Number</th>
	          <th>Image</th>
	          <th>Action</th>
	        </tr> 
	        </thead>
	        <tbody> 
	       		@foreach($customers as $customer)
	       		<tr>
	       			<th>{{$customer->id}}</th>
	       			<th>{{$customer->name}}</th>
	       			<th>{{$customer->email}}</th>
	       			<th>{{$customer->phone_number}}</th>
	       			<th></th>
	       			<td><a href="{{ route('customers.show', $customer->id) }}" class="btn btn-default btn-sm">View</a><a href="#" class="btn btn-primary btn-sm">Edit</a><a href="" class="btn btn-danger btn-sm">Delete</a></td>
	       		</tr>

	       		@endforeach

	        
	        </tbody>
	        <tfoot>
	        <tr>
	          <th>Customer ID</th>
	          <th>Name</th>
	          <th>Email</th>
	          <th>Phone Number</th>
	          <th>Image</th>
	          <th>Action</th>
	        </tr> 
	        </tfoot>
	      </table>
	    </div>
	    <!-- /.box-body -->

	    <div class="box-footer clearfix">
	      <ul class="pagination pagination-sm no-margin pull-right">
	        <li><a href="#">&laquo;</a></li>
	        <li><a href="#">1</a></li>
	        <li><a href="#">2</a></li>
	        <li><a href="#">3</a></li>
	        <li><a href="#">&raquo;</a></li>
	      </ul>
	    </div>
  </div>	  
  <!-- /.box -->
</div>
</div>
@endsection