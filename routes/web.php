<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@getIndex');

Route::get('about', "PagesController@getAbout");

Route::resource('suppliers', 'SupplierController');

Route::resource('customers','CustomerController');

Route::resource('employees','EmployeeController');

Route::resource('items', 'ItemController');

Route::resource('receivings','ReceivingController');

Route::resource('sales','SaleController');

//Route::get('/', function () {
  //  return view('welcome');
//});
